<?php

namespace Drupal\discord_webform_handler\Plugin\WebformHandler;

use Drupal\Core\Utility\Error;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "discord_webhook",
 *   label = @Translation("Discord Webhook"),
 *   category = @Translation("External"),
 *   description = @Translation("Posts webform submissions to a Discord webhook."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class DiscordWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'webhook_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['webhook_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Discord Webhook URL'),
      '#description' => $this->t('Enter the URL of the Discord webhook that you want to post to.'),
      '#default_value' => $this->configuration['webhook_url'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['webhook_url'] = $form_state->getValue('webhook_url');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $client = \Drupal::httpClient();
    $webhook_url = $this->configuration['webhook_url'];
    try {
      $client->post($webhook_url, [
        'json' => [
          'content' => json_encode($data),
        ],
      ]);
    }
    catch (RequestException $e) {
      DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.0.0', fn() => Error::logException(\Drupal::logger('webform_discord_webhook'), $e), fn() => watchdog_exception('webform_discord_webhook', $e));
    }
  }

}
